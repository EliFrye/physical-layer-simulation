//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#pragma once

#include "LineEncoder.h"
#include <list>

class AMI_HDB3_Enc : public LineEncoder {
public:
	AMI_HDB3_Enc(void);
	~AMI_HDB3_Enc(void);

	std::list<float> encode(const std::list<bool>&);
};

