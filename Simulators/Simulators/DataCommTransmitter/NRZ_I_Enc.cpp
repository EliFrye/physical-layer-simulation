//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#include "NRZ_I_Enc.h"

using namespace std;

NRZ_I_Enc::NRZ_I_Enc(void) {
}


NRZ_I_Enc::~NRZ_I_Enc(void) {
}

list<float> NRZ_I_Enc::encode(const list<bool>& in) {
	list<float> output;
	double lastVoltage=-5.0;//Arbitrary, Could have been 5.0, but must match cpp file.
	for(auto citer = in.cbegin(); citer != in.cend(); ++citer) {
		if(*citer) {
			if(lastVoltage==5.0)
			{
				output.push_back(-5.0f);
				lastVoltage = -5.0;
			}
			else if (lastVoltage==-5.0)
			{
				output.push_back(5.0f);
				lastVoltage = 5.0;
			}
		}
		else {
			if(lastVoltage==5.0)
			{
				output.push_back(5.0f);
				lastVoltage = 5.0;
			}
			else if (lastVoltage==-5.0)
			{
				output.push_back(-5.0f);
				lastVoltage = -5.0;
			}
		}
	}

	return output;
}