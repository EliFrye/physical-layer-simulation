///Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#include "2B1Q_Enc.h"

using namespace std;

_2B1Q_Enc::_2B1Q_Enc(void) {
}


_2B1Q_Enc::~_2B1Q_Enc(void) {
}

list<float> _2B1Q_Enc::encode(const list<bool>& in) {//Using voltage values from textbook.
	list<float> output;
	bool first;
	for(auto citer = in.cbegin(); citer != in.cend(); ++citer) {
		first=*citer;
		++citer;
		if(*citer && first)
			output.push_back(1.0f);
		else if(*citer && !first)
			output.push_back(-1.0f);
		else if(!*citer && first)
			output.push_back(3.0f);
		else if(!*citer && !first)
			output.push_back(-3.0f);
	}

	return output;
}