//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#include "MLT_3_Enc.h"

using namespace std;

MLT_3_Enc::MLT_3_Enc(void) {
}


MLT_3_Enc::~MLT_3_Enc(void) {
}

list<float> MLT_3_Enc::encode(const list<bool>& in) {//Using 5V, 0V, -5V
	list<float> output;
	double lastVoltageLevel=-5;
	double lastNonZeroLevel=-5;
	for(auto citer = in.cbegin(); citer != in.cend(); ++citer)
	{
		if(!*citer) 
		{
			output.push_back(lastVoltageLevel);
			if(lastVoltageLevel)
				lastNonZeroLevel=lastVoltageLevel;
		}
		else if(*citer && lastVoltageLevel)
		{
			output.push_back(0);
			lastVoltageLevel=0;
		}
		else if(*citer && !lastVoltageLevel)
		{
			output.push_back(-lastNonZeroLevel);
			lastVoltageLevel=-lastNonZeroLevel;
			lastNonZeroLevel=-lastNonZeroLevel;
		}
	}

	return output;
}