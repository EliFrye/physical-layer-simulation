//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#include "AMI_HDB3_Enc.h"

using namespace std;

AMI_HDB3_Enc::AMI_HDB3_Enc(void) {
}


AMI_HDB3_Enc::~AMI_HDB3_Enc(void) {
}

list<float> AMI_HDB3_Enc::encode(const list<bool>& in) {
	list<float> output;//Output of line encoding.
	list<float> finalOutput;//Output of scrambling.
	//AMI
	double lastNonZero = -5.0;
	for(auto citer = in.cbegin(); citer != in.cend(); ++citer) {
		if(*citer) {
			output.push_back(0.0f);
		}
		else {
			if(lastNonZero==5.0f)
			{
				output.push_back(-5.0f);
				lastNonZero=-5.0;
			}
			else
			{
				output.push_back(5.0f);
				lastNonZero=5.0;
			}
		}
	}
	//Scrambling
	int numConsecutiveZeroes = 0;
	lastNonZero = -5;
	int numNonZeroPulses = 0;//May not be the right value always because we only care about wheteher it is odd or even.
	for(auto citer2 = output.begin(); citer2 != output.end(); ++citer2){
		if(*citer2==5.0F||*citer2==-5.0F) {
			for(int i=0;i<numConsecutiveZeroes;i++)
				finalOutput.push_back(0.0f);
			finalOutput.push_back(*citer2);
			numNonZeroPulses++;
			lastNonZero = *citer2;
			numConsecutiveZeroes=0;
		}
		else
		{
			numConsecutiveZeroes++;
			if(numConsecutiveZeroes==4)
			{
				if(numNonZeroPulses%2)
				{
					finalOutput.push_back(0.0f);
					finalOutput.push_back(0.0f);
					finalOutput.push_back(0.0f);
					if(lastNonZero==5.0)
						finalOutput.push_back(5.0);
					else if(lastNonZero==-5.0)
						finalOutput.push_back(-5.0);
					++citer2;//Skip the zeroes that are being replaced.
					++citer2;
					++citer2;
				}
				else if(!(numNonZeroPulses%2))
				{
					if(lastNonZero==5.0)
					{
						finalOutput.push_back(-5.0);
						lastNonZero = -5.0;
					}
					else if(lastNonZero==-5.0)
					{
						finalOutput.push_back(5.0);
						lastNonZero = 5.0;
					}
					finalOutput.push_back(0.0f);
					finalOutput.push_back(0.0f);
					if(lastNonZero==5.0)
						finalOutput.push_back(5.0);
					else if(lastNonZero==-5.0)
						finalOutput.push_back(-5.0);
					++citer2;//Skip the zeroes that are being replaced.
					++citer2;
					++citer2;
				}
				numConsecutiveZeroes=0;
				numNonZeroPulses=0;//Not really, but we only care whether it is odd or even anyway. 
			}
		}
		for(int i = 0; i < numConsecutiveZeroes; i++)
			finalOutput.push_back(0);
	}
	
	return finalOutput;
}