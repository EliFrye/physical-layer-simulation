/************
	Eli Frye with code copied from prof. Boatright
 * Class declaration for a 4B/5B block encoder.
 * 
 * For more details see the corresponding .cpp file.
 *
 * Created 10/21/15 by Eli Frye with code copied from Dr. Boatright
 ************/

#pragma once

#include "BlockEncoder.h"
#include <forward_list>
#include <list>
#include <cmath>
const int BLOCK_SIZE = 4;//The length of a block of data.
const int DATA_VALUES_SIZE = std::pow(2.0,BLOCK_SIZE);//How many bit patterns a block can have. 
const int ENCODED_BLOCK_SIZE = 5;
class Block4B5BEnc : public BlockEncoder {
public:
	Block4B5BEnc(void);
	~Block4B5BEnc(void);

	std::list<bool> encode(const std::forward_list<bool>&);
private:
	struct dataExtendedPair
	{
		bool data[4];
		bool extended[5];
	};
	dataExtendedPair mapping[16];
};

