//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#pragma once

#include "LineEncoder.h"
#include <list>

class NRZ_I_Enc : public LineEncoder {
public:
	NRZ_I_Enc(void);
	~NRZ_I_Enc(void);

	std::list<float> encode(const std::list<bool>&);
};

