//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#include "Block4B5BEncoder.h"

using namespace std;

Block4B5BEnc::Block4B5BEnc() {
	((mapping[0]).data)[0]=false;
	((mapping[0]).data)[1]=false;
	((mapping[0]).data)[2]=false;
	((mapping[0]).data)[3]=false;
	((mapping[0]).extended)[0]=true;
	((mapping[0]).extended)[1]=true;
	((mapping[0]).extended)[2]=true;
	((mapping[0]).extended)[3]=true;
	((mapping[0]).extended)[4]=false;

	((mapping[1]).data)[0]=false;
	((mapping[1]).data)[1]=false;
	((mapping[1]).data)[2]=false;
	((mapping[1]).data)[3]=true;
	((mapping[1]).extended)[0]=false;
	((mapping[1]).extended)[1]=true;
	((mapping[1]).extended)[2]=false;
	((mapping[1]).extended)[3]=false;
	((mapping[1]).extended)[4]=true;

	((mapping[2]).data)[0]=false;
	((mapping[2]).data)[1]=false;
	((mapping[2]).data)[2]=true;
	((mapping[2]).data)[3]=false;
	((mapping[2]).extended)[0]=true;
	((mapping[2]).extended)[1]=false;
	((mapping[2]).extended)[2]=true;
	((mapping[2]).extended)[3]=false;
	((mapping[2]).extended)[4]=false;

	((mapping[3]).data)[0]=false;
	((mapping[3]).data)[1]=false;
	((mapping[3]).data)[2]=true;
	((mapping[3]).data)[3]=true;
	((mapping[3]).extended)[0]=true;
	((mapping[3]).extended)[1]=false;
	((mapping[3]).extended)[2]=true;
	((mapping[3]).extended)[3]=false;
	((mapping[3]).extended)[4]=true;

	((mapping[4]).data)[0]=false;
	((mapping[4]).data)[1]=true;
	((mapping[4]).data)[2]=false;
	((mapping[4]).data)[3]=false;
	((mapping[4]).extended)[0]=false;
	((mapping[4]).extended)[1]=true;
	((mapping[4]).extended)[2]=false;
	((mapping[4]).extended)[3]=true;
	((mapping[4]).extended)[4]=false;

	((mapping[5]).data)[0]=false;
	((mapping[5]).data)[1]=true;
	((mapping[5]).data)[2]=false;
	((mapping[5]).data)[3]=true;
	((mapping[5]).extended)[0]=false;
	((mapping[5]).extended)[1]=true;
	((mapping[5]).extended)[2]=false;
	((mapping[5]).extended)[3]=true;
	((mapping[5]).extended)[4]=true;

	((mapping[6]).data)[0]=false;
	((mapping[6]).data)[1]=true;
	((mapping[6]).data)[2]=true;
	((mapping[6]).data)[3]=false;
	((mapping[6]).extended)[0]=false;
	((mapping[6]).extended)[1]=true;
	((mapping[6]).extended)[2]=true;
	((mapping[6]).extended)[3]=true;
	((mapping[6]).extended)[4]=false;

	((mapping[7]).data)[0]=false;
	((mapping[7]).data)[1]=true;
	((mapping[7]).data)[2]=true;
	((mapping[7]).data)[3]=true;
	((mapping[7]).extended)[0]=false;
	((mapping[7]).extended)[1]=true;
	((mapping[7]).extended)[2]=true;
	((mapping[7]).extended)[3]=true;
	((mapping[7]).extended)[4]=true;

	((mapping[8]).data)[0]=true;
	((mapping[8]).data)[1]=false;
	((mapping[8]).data)[2]=false;
	((mapping[8]).data)[3]=false;
	((mapping[8]).extended)[0]=true;
	((mapping[8]).extended)[1]=false;
	((mapping[8]).extended)[2]=false;
	((mapping[8]).extended)[3]=true;
	((mapping[8]).extended)[4]=false;

	((mapping[9]).data)[0]=true;
	((mapping[9]).data)[1]=false;
	((mapping[9]).data)[2]=false;
	((mapping[9]).data)[3]=true;
	((mapping[9]).extended)[0]=true;
	((mapping[9]).extended)[1]=false;
	((mapping[9]).extended)[2]=false;
	((mapping[9]).extended)[3]=true;
	((mapping[9]).extended)[4]=true;

	((mapping[10]).data)[0]=true;
	((mapping[10]).data)[1]=false;
	((mapping[10]).data)[2]=true;
	((mapping[10]).data)[3]=false;
	((mapping[10]).extended)[0]=true;
	((mapping[10]).extended)[1]=false;
	((mapping[10]).extended)[2]=true;
	((mapping[10]).extended)[3]=true;
	((mapping[10]).extended)[4]=false;

	((mapping[11]).data)[0]=true;
	((mapping[11]).data)[1]=false;
	((mapping[11]).data)[2]=true;
	((mapping[11]).data)[3]=true;
	((mapping[11]).extended)[0]=true;
	((mapping[11]).extended)[1]=false;
	((mapping[11]).extended)[2]=true;
	((mapping[11]).extended)[3]=true;
	((mapping[11]).extended)[4]=true;

	((mapping[12]).data)[0]=true;
	((mapping[12]).data)[1]=true;
	((mapping[12]).data)[2]=false;
	((mapping[12]).data)[3]=false;
	((mapping[12]).extended)[0]=true;
	((mapping[12]).extended)[1]=true;
	((mapping[12]).extended)[2]=false;
	((mapping[12]).extended)[3]=true;
	((mapping[12]).extended)[4]=false;

	((mapping[13]).data)[0]=true;
	((mapping[13]).data)[1]=true;
	((mapping[13]).data)[2]=false;
	((mapping[13]).data)[3]=true;
	((mapping[13]).extended)[0]=true;
	((mapping[13]).extended)[1]=true;
	((mapping[13]).extended)[2]=false;
	((mapping[13]).extended)[3]=true;
	((mapping[13]).extended)[4]=true;

	((mapping[14]).data)[0]=true;
	((mapping[14]).data)[1]=true;
	((mapping[14]).data)[2]=true;
	((mapping[14]).data)[3]=false;
	((mapping[14]).extended)[0]=true;
	((mapping[14]).extended)[1]=true;
	((mapping[14]).extended)[2]=true;
	((mapping[14]).extended)[3]=false;
	((mapping[14]).extended)[4]=false;

	((mapping[15]).data)[0]=true;
	((mapping[15]).data)[1]=true;
	((mapping[15]).data)[2]=true;
	((mapping[15]).data)[3]=true;
	((mapping[15]).extended)[0]=true;
	((mapping[15]).extended)[1]=true;
	((mapping[15]).extended)[2]=true;
	((mapping[15]).extended)[3]=false;
	((mapping[15]).extended)[4]=true;
}


Block4B5BEnc::~Block4B5BEnc() {
}

list<bool> Block4B5BEnc::encode(const forward_list<bool>& raw_stream) {
	bool dataBlock[4];
	list<bool> output;
	int curIndex=0;//The next index in block to write to.
	for(auto citer = raw_stream.cbegin(); citer != raw_stream.cend(); ++citer)
	{
		dataBlock[curIndex]=*citer;
		curIndex=(curIndex+1)%4;
		if(!curIndex)
		{
			int foundIndex;//Tells what the index in mapping is that our data block cooresponds to.
			for(int i = 0;i < DATA_VALUES_SIZE;i++)
			{
				bool isMatch=true;
				for(int j = 0;j < BLOCK_SIZE;j++)
					if(dataBlock[j]!=mapping[i].data[j])
					{
						isMatch = false;
						break;
					}
				if(isMatch)
				{
					foundIndex=i;
					break;
				}
			}
			for(int i=0;i<ENCODED_BLOCK_SIZE;i++)
				output.push_back((mapping[foundIndex]).extended[i]);
		}
	}
	return output;
}