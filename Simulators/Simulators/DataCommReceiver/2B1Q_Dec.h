//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015
#pragma once

#include "LineDecoder.h"

class _2B1Q_Dec : public LineDecoder {
public:
	_2B1Q_Dec(void);
	~_2B1Q_Dec(void);

	std::list<bool> decode(const std::list<float>&);
};

