//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#include "2B1Q_Dec.h"

using namespace std;

_2B1Q_Dec::_2B1Q_Dec() {
}

_2B1Q_Dec::~_2B1Q_Dec() {
}

list<bool> _2B1Q_Dec::decode(const list<float>& stream) {
	list<bool> output;
	for(auto citer = stream.cbegin(); citer != stream.cend(); ++citer) {
		if(*citer >= 2)
		{
			output.push_back(true);
			output.push_back(false);
		}
		else if(*citer <= -2)
		{
			output.push_back(false);
			output.push_back(false);
		}
		else if(*citer >= -2 && *citer <= 0)
		{
			output.push_back(false);
			output.push_back(true);
		}
		else if(*citer >= 0 && *citer <= 2)
		{
			output.push_back(true);
			output.push_back(true);
		}
	}

	return output;
}