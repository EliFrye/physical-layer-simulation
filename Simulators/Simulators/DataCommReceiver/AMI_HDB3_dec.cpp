//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#include "AMI_HDB3_Dec.h"

using namespace std;

AMI_HDB3_dec::AMI_HDB3_dec() {
}

AMI_HDB3_dec::~AMI_HDB3_dec() {
}

list<bool> AMI_HDB3_dec::decode(const list<float>& stream) {
	list<double> output;//Result of undoing scrambling.
	list<bool> finalOutput;//Result of undoing line encoding.
	bool lastNonZero = false;//True if positive.
	int numZeros=0;
	double curVal;
	//undo scrambling
	double prevVals[3];
	for(auto citer = stream.cbegin(); citer != stream.cend(); ++citer)
	{
		if(*citer >= -2.5f&&*citer<=2.5f)
			curVal=0.0f;
		else if(*citer<-2.5)
			curVal = -5.0f;
		else
			curVal = 5.0f;
		if((curVal==5&&lastNonZero==-5)||(curVal==-5&&lastNonZero==5))
		{
			output.push_back(0);
			output.push_back(0);
			output.push_back(0);
			output.push_back(0);
			curVal=0x00;
			for(int i=0;i<3;i++)
			{
				prevVals[i]=0x00;
			}
		}
		else
		{
			output.push_back(prevVals[2]);
			prevVals[2]=prevVals[1];
			prevVals[1]=prevVals[0];
			prevVals[0]=curVal;
		}
		if(curVal!=0)
			lastNonZero=curVal;
	}
		for(int i=2;i>=0;i--)
		{
			if(prevVals[i])
				output.push_back(prevVals[i]);
		}
		if(curVal)
			output.push_back(curVal);
	/*for(auto citer = stream.cbegin(); citer != stream.cend(); ++citer) {
		if(*citer <= -2.5f||*citer>=2.5f)
		{
			numZeros++;
			if(numZeros==3)
			{
				output.push_back(0);
				numZeros--;
			}
		}
		else if((*citer <= -2.5f) && lastNonZero)
		{
			output.push_back(-5);
			lastNonZero = false;
		}
		else if((*citer >= 2.5f) && !lastNonZero)
		{
			output.push_back(5);
			lastNonZero = true;
		}
		else
		{
			if(numZeros==3)
			{
				output.push_back(0);
				output.push_back(0);
				output.push_back(0);
				output.push_back(0);
				numZeros=0;
			}
			else
			{
				output.push_back(0);

			}
		}
		output.push_back(*citer <= -2.5f||*citer>=2.5f);
	}*/
	//undo line encoding
	for(auto citer = output.cbegin(); citer != output.cend(); ++citer) {
		finalOutput.push_back(*citer <= -2.5f||*citer>=2.5f);
	}

	return finalOutput;
}