//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015
#include "MLT_3_Dec.h"

using namespace std;

MLT_3_Dec::MLT_3_Dec() {
}

MLT_3_Dec::~MLT_3_Dec() {
}

list<bool> MLT_3_Dec::decode(const list<float>& stream) {
	list<bool> output;
	double lastLevel=-5.0f;
	for(auto citer = stream.cbegin(); citer != stream.cend(); ++citer) {
		output.push_back(!((lastLevel<=-2.5&&*citer<=-2.5)||(lastLevel>=2.5&&*citer>=2.5)||(lastLevel>-2.5&&lastLevel<2.5&&*citer>-2.5&&*citer<2.5)));
		lastLevel=*citer;
	}

	return output;
}