/************
 * Example block decoder which simply passes data through.
 *
 * To create your own block decoder, you will need to make another child class of BlockDecoder and override the decode function.
 * Since this is as simple as it gets, there are some things to keep in mind when you implement your own:
 *     - You will likely want a table of blocks for substituting values.
 *	   - You will have to read more than one bit at a time to match against the table of blocks.
 *	   - The length of list that is output should NOT be the length of the input list!
 *
 * Created 9/21/2014 by Cory D. Boatright for use in COMP 342 at Grove City College
 ************/

#include "Block4B5BDecoder.h"

using namespace std;

Block4B5BDec::Block4B5BDec() {
	((mapping[0]).data)[0]=false;
	((mapping[0]).data)[1]=false;
	((mapping[0]).data)[2]=false;
	((mapping[0]).data)[3]=false;
	((mapping[0]).extended)[0]=true;
	((mapping[0]).extended)[1]=true;
	((mapping[0]).extended)[2]=true;
	((mapping[0]).extended)[3]=true;
	((mapping[0]).extended)[4]=false;

	((mapping[1]).data)[0]=false;
	((mapping[1]).data)[1]=false;
	((mapping[1]).data)[2]=false;
	((mapping[1]).data)[3]=true;
	((mapping[1]).extended)[0]=false;
	((mapping[1]).extended)[1]=true;
	((mapping[1]).extended)[2]=false;
	((mapping[1]).extended)[3]=false;
	((mapping[1]).extended)[4]=true;

	((mapping[2]).data)[0]=false;
	((mapping[2]).data)[1]=false;
	((mapping[2]).data)[2]=true;
	((mapping[2]).data)[3]=false;
	((mapping[2]).extended)[0]=true;
	((mapping[2]).extended)[1]=false;
	((mapping[2]).extended)[2]=true;
	((mapping[2]).extended)[3]=false;
	((mapping[2]).extended)[4]=false;

	((mapping[3]).data)[0]=false;
	((mapping[3]).data)[1]=false;
	((mapping[3]).data)[2]=true;
	((mapping[3]).data)[3]=true;
	((mapping[3]).extended)[0]=true;
	((mapping[3]).extended)[1]=false;
	((mapping[3]).extended)[2]=true;
	((mapping[3]).extended)[3]=false;
	((mapping[3]).extended)[4]=true;

	((mapping[4]).data)[0]=false;
	((mapping[4]).data)[1]=true;
	((mapping[4]).data)[2]=false;
	((mapping[4]).data)[3]=false;
	((mapping[4]).extended)[0]=false;
	((mapping[4]).extended)[1]=true;
	((mapping[4]).extended)[2]=false;
	((mapping[4]).extended)[3]=true;
	((mapping[4]).extended)[4]=false;

	((mapping[5]).data)[0]=false;
	((mapping[5]).data)[1]=true;
	((mapping[5]).data)[2]=false;
	((mapping[5]).data)[3]=true;
	((mapping[5]).extended)[0]=false;
	((mapping[5]).extended)[1]=true;
	((mapping[5]).extended)[2]=false;
	((mapping[5]).extended)[3]=true;
	((mapping[5]).extended)[4]=true;

	((mapping[6]).data)[0]=false;
	((mapping[6]).data)[1]=true;
	((mapping[6]).data)[2]=true;
	((mapping[6]).data)[3]=false;
	((mapping[6]).extended)[0]=false;
	((mapping[6]).extended)[1]=true;
	((mapping[6]).extended)[2]=true;
	((mapping[6]).extended)[3]=true;
	((mapping[6]).extended)[4]=false;

	((mapping[7]).data)[0]=false;
	((mapping[7]).data)[1]=true;
	((mapping[7]).data)[2]=true;
	((mapping[7]).data)[3]=true;
	((mapping[7]).extended)[0]=false;
	((mapping[7]).extended)[1]=true;
	((mapping[7]).extended)[2]=true;
	((mapping[7]).extended)[3]=true;
	((mapping[7]).extended)[4]=true;

	((mapping[8]).data)[0]=true;
	((mapping[8]).data)[1]=false;
	((mapping[8]).data)[2]=false;
	((mapping[8]).data)[3]=false;
	((mapping[8]).extended)[0]=true;
	((mapping[8]).extended)[1]=false;
	((mapping[8]).extended)[2]=false;
	((mapping[8]).extended)[3]=true;
	((mapping[8]).extended)[4]=false;

	((mapping[9]).data)[0]=true;
	((mapping[9]).data)[1]=false;
	((mapping[9]).data)[2]=false;
	((mapping[9]).data)[3]=true;
	((mapping[9]).extended)[0]=true;
	((mapping[9]).extended)[1]=false;
	((mapping[9]).extended)[2]=false;
	((mapping[9]).extended)[3]=true;
	((mapping[9]).extended)[4]=true;

	((mapping[10]).data)[0]=true;
	((mapping[10]).data)[1]=false;
	((mapping[10]).data)[2]=true;
	((mapping[10]).data)[3]=false;
	((mapping[10]).extended)[0]=true;
	((mapping[10]).extended)[1]=false;
	((mapping[10]).extended)[2]=true;
	((mapping[10]).extended)[3]=true;
	((mapping[10]).extended)[4]=false;

	((mapping[11]).data)[0]=true;
	((mapping[11]).data)[1]=false;
	((mapping[11]).data)[2]=true;
	((mapping[11]).data)[3]=true;
	((mapping[11]).extended)[0]=true;
	((mapping[11]).extended)[1]=false;
	((mapping[11]).extended)[2]=true;
	((mapping[11]).extended)[3]=true;
	((mapping[11]).extended)[4]=true;

	((mapping[12]).data)[0]=true;
	((mapping[12]).data)[1]=true;
	((mapping[12]).data)[2]=false;
	((mapping[12]).data)[3]=false;
	((mapping[12]).extended)[0]=true;
	((mapping[12]).extended)[1]=true;
	((mapping[12]).extended)[2]=false;
	((mapping[12]).extended)[3]=true;
	((mapping[12]).extended)[4]=false;

	((mapping[13]).data)[0]=true;
	((mapping[13]).data)[1]=true;
	((mapping[13]).data)[2]=false;
	((mapping[13]).data)[3]=true;
	((mapping[13]).extended)[0]=true;
	((mapping[13]).extended)[1]=true;
	((mapping[13]).extended)[2]=false;
	((mapping[13]).extended)[3]=true;
	((mapping[13]).extended)[4]=true;

	((mapping[14]).data)[0]=true;
	((mapping[14]).data)[1]=true;
	((mapping[14]).data)[2]=true;
	((mapping[14]).data)[3]=false;
	((mapping[14]).extended)[0]=true;
	((mapping[14]).extended)[1]=true;
	((mapping[14]).extended)[2]=true;
	((mapping[14]).extended)[3]=false;
	((mapping[14]).extended)[4]=false;

	((mapping[15]).data)[0]=true;
	((mapping[15]).data)[1]=true;
	((mapping[15]).data)[2]=true;
	((mapping[15]).data)[3]=true;
	((mapping[15]).extended)[0]=true;
	((mapping[15]).extended)[1]=true;
	((mapping[15]).extended)[2]=true;
	((mapping[15]).extended)[3]=false;
	((mapping[15]).extended)[4]=true;
}

Block4B5BDec::~Block4B5BDec() {
}

list<bool> Block4B5BDec::decode(const list<bool>& stream) {
	list<bool> output;
	bool encodedBlock[ENCODED_BLOCK_SIZE];
	int index=0;
	for(auto citer = stream.cbegin(); citer != stream.cend(); ++citer) {
		encodedBlock[index]=*citer;
		//output.push_back(*citer);
		index = (index+1)%ENCODED_BLOCK_SIZE;
		if(!index)
		{
			int foundIndex;//Tells what the index in mapping is that our encoded block cooresponds to.
			for(int i = 0;i < DATA_VALUES_SIZE;i++)
			{
				bool isMatch=true;
				for(int j = 0;j < ENCODED_BLOCK_SIZE;j++)
					if(encodedBlock[j]!=mapping[i].extended[j])
					{
						isMatch = false;
						break;
					}
				if(isMatch)
				{
					foundIndex=i;
					break;
				}
			}
			for(int i=0;i<BLOCK_SIZE;i++)
				output.push_back((mapping[foundIndex]).data[i]);
		}
	}
	return output;
}