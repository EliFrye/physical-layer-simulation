/************
 * Class declaration for a pass-through block decoder.
 *
 * For more details see the corresponding .cpp file.
 *
 * Created 9/21/2014 by Cory D. Boatright for use in COMP 342 at Grove City College
 ************/

#pragma once

#include "BlockDecoder.h"
#include <cmath>
const int BLOCK_SIZE = 4;//The length of a block of data.
const int DATA_VALUES_SIZE = std::pow(2.0,BLOCK_SIZE);//How many bit patterns a block can have. 
const int ENCODED_BLOCK_SIZE = 5;
class Block4B5BDec : public BlockDecoder {
public:
	Block4B5BDec(void);
	~Block4B5BDec(void);

	std::list<bool> decode(const std::list<bool>&);
private:
	struct dataExtendedPair
	{
		bool data[4];
		bool extended[5];
	};
	dataExtendedPair mapping[16];
};

