//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#pragma once

#include "LineDecoder.h"

class AMI_HDB3_dec : public LineDecoder {
public:
	AMI_HDB3_dec(void);
	~AMI_HDB3_dec(void);

	std::list<bool> decode(const std::list<float>&);
};

