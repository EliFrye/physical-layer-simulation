//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#include "NRZ_I_Dec.h"

using namespace std;

NRZ_I_Dec::NRZ_I_Dec() {
}

NRZ_I_Dec::~NRZ_I_Dec() {
}

list<bool> NRZ_I_Dec::decode(const list<float>& stream) {
	list<bool> output;
	bool lastVoltage=false;
	for(auto citer = stream.cbegin(); citer != stream.cend(); ++citer) {
		output.push_back(lastVoltage!=(*citer>2.5f));
		lastVoltage= (*citer>2.5f);
	}
	return output;
}