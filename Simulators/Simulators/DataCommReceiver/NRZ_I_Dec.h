//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#pragma once

#include "LineDecoder.h"

class NRZ_I_Dec : public LineDecoder {
public:
	NRZ_I_Dec(void);
	~NRZ_I_Dec(void);

	std::list<bool> decode(const std::list<float>&);
};

