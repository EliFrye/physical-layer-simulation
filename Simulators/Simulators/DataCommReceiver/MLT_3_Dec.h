//Dr. Boatright and Eli Frye
//Grove City College
//Comp 342
//Fall 2015

#pragma once

#include "LineDecoder.h"

class MLT_3_Dec : public LineDecoder {
public:
	MLT_3_Dec(void);
	~MLT_3_Dec(void);

	std::list<bool> decode(const std::list<float>&);
};

